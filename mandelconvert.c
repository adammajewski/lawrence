// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include "mandel.h"

int
main(int argc,char **argv)
{
  complex double z;
  double re,im;
  int c;
  FILE *fp=fopen("/dev/stdin","rb");

  while((c=getopt(argc,argv,"m:h"))!=-1)
    switch(c)
      {
      case 'h':
        fprintf(stderr,"modes: b -- binary to float\n       f -- float to binary\n");
        break;
      case 'm':
        if(strcmp(optarg,"b")==0)
          {
            fp=fopen("/dev/stdin","rb");
            while((c=fread(&z,sizeof(complex double),1,fp))!=0)
              {
                printf("%.16f %.16f\n",creal(z),cimag(z));
              }
          }
        else if(strcmp(optarg,"f")==0)
          {
            fp=fopen("/dev/stdout","wb");
            while((c=fscanf(stdin,"%lf %lf",&re,&im))!=EOF)
              {
                z=re+I*im;
                fwrite(&z,sizeof(complex double),1,fp);
              }
          }
        else
          {
            abort();
          }
        break;
      case '?':
        abort();
      default:
        abort();
      }
  


  return 0;
}
