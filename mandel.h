// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<complex.h>

#define CMPLX(c) creal(c),cimag(c)

double complex mandel(int k,double complex z);
int mandeld(int k,double complex z,double complex *pout,double complex *dpout);
int mandeld3(int k,double complex z,double complex *pout,double complex *dpout,\
             double complex *d2pout,double complex *d3pout);
int mandelread(complex double **buf,char *filename);
int mandelwrite(int count,complex double *buf,char *filename);
