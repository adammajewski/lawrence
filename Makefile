#CFLAGS=-g

#OpenMP can be used to parallelize the the loop over all points
#CFLAGS= -fopenmp

#CFLAGS+= -DPATH

PROG=mandelhom mandeleuler mandelconvert
OBJ=mandeleuler.o mandel.o mandelutil.o mandelconvert.o

all: $(PROG)

mandelhom: mandelhom.o mandel.o mandelutil.o
	$(CC) $(CFLAGS) -o $@ $^ -lgsl -lgslcblas -lm

mandeleuler: mandeleuler.o mandel.o
	$(CC) $(CFLAGS) -o $@ $^ -lgsl -lgslcblas -lm

mandelconvert: mandelconvert.o
	$(CC) $(CFLAGS) -o $@ $^ 

.PHONY:clean
clean:
	rm -f $(OBJ)

.PHONY:clean-all
clean-all: clean
	rm -f $(PROG)
