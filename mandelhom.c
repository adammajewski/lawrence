// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_odeiv.h>
#include<assert.h>
#include<complex.h>
#include<unistd.h>
#include<string.h>
#include<math.h>
#include"mandel.h"

#define BUFLEN 2048
#define NEWTON_ITER 5
#define NEWTON_TOL 1e-10
#define SER_STEP 1e-2
#define INI_STEP 1e-3
#define INIT_TOL 1e-5
#define INT_TOL_ABS 1e-9
#define INT_TOL_REL 1e-6
#define NEWTON_FINAL 10
#define NEWTON_FINAL_TOL 1e-13

#define THETA M_PI/16
#define GAMMA cexp(THETA*I)

int
mandelhom(double t,const double y[],double f[],void*params)
{
  /* Homotopy for mandelbrot polynomial */
  int k;
  complex double z,p,dp;
  
  // Extract evaluation point from vector data
  z = y[0] + I*y[1];

  // Extract k
  k = *(int*)params;

  // Evaluate derivative
  // dxi/dt = -2*t/p_k'(xi)
  mandeld(k,z,&p,&dp);
  p = -2.0*(GAMMA*t/(1.0+(GAMMA-1)*t))/dp;

  // Put differential equation back into vector form
  f[0]=creal(p);
  f[1]=cimag(p);
  return GSL_SUCCESS;
}

double complex
mandelinit(int k,double h,complex double z,double branch)
{
  /* Initialization function from series */
  // Picks appropriate stepsize ensuring that the residual is small (maybe)

  complex double t,p,dp,d2p,d3p,z2,z3;
  complex double beta[3];
  double r2,r3;

  
  // Compute series coefficients (don't change if we vary the step length)
  mandeld3(k-1,z,&p,&dp,&d2p,&d3p);
  beta[0]=branch*csqrt(-1.0/(z*dp*dp));
  beta[1]=-(beta[0]*beta[0]*(dp+z*d2p)/(2.0*z*dp));
  beta[2]=-(beta[0]*beta[0]*beta[0]*                                    \
            (-15.0*dp*dp-18.0*z*dp*d2p-12.0*z*z*d2p+4.0*z*z*dp*d3p))/(24.0*z*z*dp*dp);

  h*=2.0;
  do
    {
      h/=2.0;
      // Evaluate point on the path
      t=(GAMMA*h/(1.0+(GAMMA-1)*h));
      
      // evaluate series with 2 and 3 terms (via Horner's method)
      z2=z+(beta[0]+beta[1]*t)*t;
      z3=z+(beta[0]+(beta[1]+beta[2]*t)*t)*t;

      // Evaluate polynomial twice
      mandeld(k,z2,&p,&dp);
      mandeld(k,z3,&d2p,&d3p);

      //Evaluate residual
      r2 = cabs(p-1.0+t*t);
      r3 = cabs(d2p-1.0+t*t);
      //printf("%g %g %g\n",h,r2,r3);
    }
  while(r3>INIT_TOL && h>1e-13);

  return z3;
}

int
mandelhomzero(double t,const double y[],double f[],void*params)
{
  /* Homotopy for mandelbrot polynomial */
  // Could be made more efficient by removing the test for being on
  // the initial point, but not sure how to integrate this with the GSL
  int k;
  complex double z,p,dp;

  z = y[0] + I*y[1];
  k=*(int*)params;

  mandeld(k,z,&p,&dp);
  p = -2.0*(GAMMA*t/(1.0+(GAMMA-1)*t))/dp;
  f[0]=creal(p);
  f[1]=cimag(p);
  return GSL_SUCCESS;
}

complex double
mandelodezero(int k,FILE *outfile)
{  
  int maxiter=NEWTON_ITER,iter,tol=NEWTON_TOL,status;
  double t=0.0, t1=1.0 ;
  double h = INI_STEP; // Initial stepsize
  double y[2];
  complex double z,p,dp,tt,dz;

  // Set initial point
  y[0]=0.0;
  y[1]=0.0;

  // Initialize ODE solver
  const gsl_odeiv_step_type * T = gsl_odeiv_step_rk8pd;
  gsl_odeiv_step *s = gsl_odeiv_step_alloc(T, 2);
  gsl_odeiv_control *c = gsl_odeiv_control_y_new(INT_TOL_ABS,INT_TOL_REL);
  gsl_odeiv_evolve *e = gsl_odeiv_evolve_alloc(2);
  gsl_odeiv_system sys = {mandelhomzero, NULL, 2, &k};

  // Start integrating
  while (t < t1)
    {
      // Take one step of the ODE solver
      status=gsl_odeiv_evolve_apply(e, c, s,&sys,&t, t1,&h, y);
      
      // Run newton iteration on solution
      z=y[0]+I*y[1];
      tt=(GAMMA*t/(1.0+(GAMMA-1)*t));
      for(iter=0;iter<maxiter;iter++)
        {
          mandeld(k-1,z,&p,&dp);
          dz=-(z*p*p+tt*tt)/(p*(p+2.0*z*dp));
          z+=dz;
          //printf ("%.16e %.16e %.16e\n", t, creal(z),cimag(z));
          if(cabs(dz)<tol)
            {
              break;
            }
        }
      y[0]=creal(z);
      y[1]=cimag(z);

      // Test for non convergence
      if (status != GSL_SUCCESS)
        {
          fprintf(stderr,"ERROR: failed to integrate\n");
          break;
        }
      // Print path to file
      //fwrite(y,sizeof(double),2,outfile);
      fprintf(outfile,"%.16e %.16e %.16e\n", t, y[0], y[1]);
    }

  // Double linefeed for gnuplot plotting
  //z=1.0/0.0+I*1.0/0.0;
  //fwrite(&z,sizeof(complex double),1,outfile);
  //fwrite(&z,sizeof(complex double),1,outfile);
  //fwrite("\n\n",sizeof(char),2,outfile);
  fprintf(outfile,"\n\n");
  //*z0=y[0]+I*y[1];

  // Clean up solver
  gsl_odeiv_evolve_free (e);
  gsl_odeiv_control_free (c);
  gsl_odeiv_step_free (s);
  return y[0]+I*y[1];
}

//intmandel

complex double
mandelodesolve(int k,complex double *z0,double sg,FILE *outfile)
{  
  int maxiter=2,iter,tol=NEWTON_TOL,status;
  double t=0.0, t1=1.0;
  double h = INI_STEP;
  double y[2];

  complex double z,p,dp,tt,dz;

  z=mandelinit(k,h,*z0,sg);
  // Set initial point
  y[0]=creal(z);
  y[1]=cimag(z);

  // Initialize ODE solver
  const gsl_odeiv_step_type * T = gsl_odeiv_step_rk8pd;
  gsl_odeiv_step *s = gsl_odeiv_step_alloc(T, 2);
  gsl_odeiv_control *c = gsl_odeiv_control_y_new(INT_TOL_ABS,INT_TOL_REL);
  gsl_odeiv_evolve *e = gsl_odeiv_evolve_alloc(2);
  gsl_odeiv_system sys = {mandelhom, NULL, 2, &k};

  // Start integrating
  while (t < t1)
    {
      // Take one step of the ODE solver
      status=gsl_odeiv_evolve_apply(e, c, s,&sys,&t, t1,&h, y);
      
      // Run newton iteration on solution
      z=y[0]+I*y[1];
      tt=(GAMMA*t/(1.0+(GAMMA-1)*t));
      for(iter=0;iter<maxiter;iter++)
        {
          mandeld(k-1,z,&p,&dp);
          dz=-(z*p*p+tt*tt)/(p*(p+2.0*z*dp));
          z+=dz;
          //printf ("%.16e %.16e %.16e\n", t, creal(z),cimag(z));
          if(cabs(dz)<tol)
            {
              break;
            }
        }
      y[0]=creal(z);
      y[1]=cimag(z);

      // Test for non convergence
      if (status != GSL_SUCCESS)
        {
          fprintf(stderr,"Did not converge\n");
          break;
        }
      // Print path to file
      //fwrite(y,sizeof(double),2,outfile);
      fprintf(outfile,"%.16e %.16e %.16e\n", t, y[0], y[1]);
    }

  // Double linefeed for gnuplot plotting
  //z=1.0/0.0+I*1.0/0.0;
  //fwrite(&z,sizeof(complex double),1,outfile);
  //fwrite(&z,sizeof(complex double),1,outfile);
  fprintf(outfile,"\n\n");
  //*z0=y[0]+I*y[1];

  // Clean up solver
  gsl_odeiv_evolve_free (e);
  gsl_odeiv_control_free (c);
  gsl_odeiv_step_free (s);
  return y[0]+I*y[1];
}



int
main(int argc,char **argv)
{
  complex double z;
  int k,i;
  int c,nlines,iter;
  char *iname=NULL,*pname=NULL,*oname;
  complex double *zbuf,*zstar;
  complex double p,dp,dz;
  FILE *fpi,*fpo,*fpp;

  
  // Exit if no k given
  if(argc<4)
    {
      fprintf(stderr,"Usage:\n\tspmandel -k period_of_points -i filename_initial_points...\n -o filename_final_values -p filename_paths\n");
      return -1;
    }

  while((c=getopt(argc,argv,"k:i:o:p:"))!=-1)
    switch(c)
      {
      case 'k':
        k=atol(optarg);
        break;
      case 'i':
        iname=malloc((strlen(optarg)+1)*sizeof(*iname));
        strcpy(iname,optarg);
        break;
      case 'o':
        oname=malloc((strlen(optarg)+1)*sizeof(*oname));
        strcpy(oname,optarg);
        break;
      case 'p':
        pname=malloc((strlen(optarg)+1)*sizeof(*pname));
        strcpy(pname,optarg);
        break;
      case '?':
        if (optopt == 'k' || optopt == 'f' || optopt == 'p')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr,"Unknown option character `\\x%x'.\n",optopt);
        return -1;
      default:
        abort();
      }

  // Read in values from binary file
  nlines=mandelread(&zbuf,iname);
  fprintf(stderr,"Read %d initial values from %s\n",nlines,iname);
  // Finished reading in initial values

  assert(zstar=malloc(sizeof(complex double)*(2*nlines+1)));

  // Start computing stuff
  fpp=fopen(pname,"w");
  //  OpenMP can be used here (possibly) to parallelize the iteration
  //  over all starting points.
  //#pragma omp parallel for
  for(i=0;i<nlines;i++)
    {
      z=zbuf[i];
      zstar[2*i]=mandelodesolve(k,&z,1.0,fpp);
      //Run newton iteration on final value
      for(iter=0;iter<NEWTON_FINAL;iter++)
        {
          mandeld(k,zstar[2*i],&p,&dp);
          dz=-p/dp;
          zstar[2*i]+=dz;
          //printf ("%.16e %.16e %.16e\n", t, creal(z),cimag(z));
          if(cabs(dz)<NEWTON_FINAL_TOL)
            {
              //printf("res: %g\n",cabs(dz));
              break;
            }
        }
      if(isnan(cabs(zstar[2*i])))
        {
          fprintf(stderr,"singular plus: %.16e %.16e (%g %g)\n",creal(z),cimag(z),creal(zstar[2*i]),cimag(zstar[2*i]));
        }
      zstar[2*i+1]=mandelodesolve(k,&z,-1.0,fpp);
      // Run Newton iteration on final value
      for(iter=0;iter<NEWTON_FINAL;iter++)
        {
          mandeld(k,zstar[2*i+1],&p,&dp);
          dz=-p/dp;
          zstar[2*i+1]+=dz;
          //printf ("%.16e %.16e %.16e\n", t, creal(z),cimag(z));
          if(cabs(dz)<NEWTON_FINAL_TOL)
            {
              break;
            }
        }
      if(isnan(cabs(zstar[2*i+1])))
        {
          fprintf(stderr,"singular minus: %.16e %.16e (%g %g)\n",creal(z),cimag(z),creal(zstar[2*i+1]),cimag(zstar[2*i+1]));
        }
    }
  // Zero point
  zstar[2*nlines]=mandelodezero(k,fpp);

  // Close the path stream
  fclose(fpp);
  // Now we have all the values, we will write the file in binary
  mandelwrite(2*nlines+1,zstar,oname);

  free(zbuf);
  free(zstar);
  free(iname);
  free(oname);
  free(pname);
  return 0;
}
