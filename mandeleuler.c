// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_odeiv.h>
#include<assert.h>
#include<complex.h>
#include<math.h>
#include"mandel.h"



//#define PRINT

double complex
mandeleuler(int k,double complex z0,double branch)
{
  int i,j;
  int iter, maxiter=5;
  double t=0.0,tf=1.0,ts;
  int n=1000;
  double h=1.0/(double)n;
  int ns=100;
  double hs=M_PI/(double)ns;
  double rho;
  double complex p,dp,si,sf,dz,z=z0,zold;
  double tol = 1e-14;
  //printf("p: %g%+gI dp: %g%+gI\n",creal(p),cimag(p),creal(dp),cimag(dp));
  int spass=0;
  // Take initial step and test for singularity
  mandeld(k-1,z,&p,&dp);
  si=p+2.0*z*dp;
  dz=branch*csqrt(-1.0/(z*dp*dp));
  zold=z;
  z+=h*dz;
  //Evaluate singularity function
  mandeld(k-1,z,&p,&dp);
  sf=p+2.0*z*dp;
  //printf("s: %g %g\n",CMPLX(si*sf));
  if(creal(sf)*creal(si)<0.0)
    {
      //spass=!spass;
      printf("Quick return si:(%g,%g) sf: (%g,%g)\n",CMPLX(si),CMPLX(sf));
      // Quick return (deal with it later
      //return 0.0/0.0;
    }

  // Here we will take a fixed number of steps
  for(i=1;i<=n;i++)
    {
      t=(double)i*h;
      // Evaluate polynomial for derivative
      mandeld(k-1,z,&p,&dp);
      si=p+2.0*z*dp;
      dz=-2*t/(p*si);
      
      // Save old z if we encounter a singularity
      zold=z;
      // Take a Euler step
      z+=h*dz;

      //Evaluate singularity function
      mandeld(k-1,z,&p,&dp);
      sf=p+2.0*z*dp;

#ifdef PRINT 
      printf("dpk: %g\n",cabs(p*sf));
#endif

      // If we have passed through a singularity, then we need to backtrack
      if(!spass & creal(si)*creal(sf)<0.0)
        {
#ifdef PRINT
          printf("Singularity in [%g,%g]\n",t,t+h);
#endif
          // We stepped through a singularity
          //(1) backtrack (t is still the original value
          z=zold;
          //radius for excursion into the complex plane
          rho=h/2;
          //(2) integrate in the complex plane [0,pi]
          for(j=0;j<ns;j++)
            {
              ts=t+h/2.0-h/2.0*cexp(-I*(double)j*hs);
              mandeld(k-1,z,&p,&dp);
              dz=-I*rho*cexp(-I*(double)j*hs)*2*t/(p*(p+2*z*dp));
              z+=hs*dz;
              printf("ts %g %g\n",CMPLX(z));
            }
          // By the end of this we have stepped around our singularity
          spass=!spass;
        }
        
      //printf("s: %g%+gI\n",CMPLX(s));
#ifdef PRINT
      printf("t: %g z: (%g,%g) s: (%g,%g) \n",t,CMPLX(z),CMPLX(si*sf));
#endif
      // Refine via newton
      for(iter=0;iter<maxiter;iter++)
        {
          zold=z;
          mandeld(k-1,z,&p,&dp);
          si=p+2.0*z*dp;
          //Apply newtons method
          dz=-(z*p*p+t*t)/(p*si);
          z+=dz;
  
          if(cabs(dz)<=tol)//*cabs(z))
            {
#ifdef PRINT
              printf("iter:%d z: (%g,%g)\n",iter,CMPLX(z));
#endif
              //newton refined
              break;
            }


#ifdef PRINT
          printf("s: %g%+gI\n",CMPLX(si));
#endif
          
          //Evaluate singularity function
          mandeld(k-1,z,&p,&dp);
          sf=p+2.0*z*dp;
          // If we have passed through a singularity, then we need to backtrack
          if(!spass & creal(si)*creal(sf)<0.0)
            {
#ifdef PRINT
              printf("Singularity in [%g,%g]\n",t,t+h);
#endif
              // We stepped through a singularity
              //(1) backtrack (t is still the original value
              z=zold;
              //radius for excursion into the complex plane
              rho=h/2;
              //(2) integrate in the complex plane [0,pi]
              for(j=0;j<ns;j++)
                {
                  ts=t+h/2.0-h/2.0*cexp(-I*(double)j*hs);
                  mandeld(k-1,z,&p,&dp);
                  dz=-I*rho*cexp(-I*(double)j*hs)*2*t/(p*(p+2*z*dp));
                  z+=hs*dz;
                  printf("ts %g %g\n",CMPLX(z));
                }
              // By the end of this we have stepped around our singularity
              spass=!spass;
            }


        }
#ifndef PRINT
      //mandeld(k,z,&p,&dp);
      printf("%g %g %g %g\n",t,CMPLX(z),cabs(sf));
#endif

      /* // Increment time step and evaluate derivative */
      /* t+=h; */
 
    }
  return z;
}

int
main(void)
{
  int k=9;

  //double complex z = -1.310702641336832563,p,dp;
  double complex z=-1.62792906520433789 +0.02214573651724719*I,p,dp;
#ifdef PRINT  
  printf("k: %d\n",k);
  printf("Initial point: %g %+gI\n",creal(z),cimag(z));
#endif
  //mandeld(k-1,z,&p,&dp);
  //printf("p: %g%+gI dp: %g%+gI\n",creal(p),cimag(p),creal(dp),cimag(dp));  
  z=mandeleuler(k,z,1.0);

#ifdef PRINT
  printf("Final point: : %g %+gI\n",creal(z),cimag(z));
  mandeld(k,z,&p,&dp);
  printf("p: %g%+gI dp: %g%+gI\n",creal(p),cimag(p),creal(dp),cimag(dp));  
#endif
  return 0;
}
