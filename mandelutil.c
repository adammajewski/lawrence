// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include "mandel.h"

int
mandelread(complex double **buf,char *filename)
{
  // mandelread reads an unknown number of data points in binary
  // format into a buffer
  FILE *fp;
  int size;

  assert(fp=fopen(filename,"rb"));
  
  // Seek to end of file to get size, then come back to end
  fseek(fp,0,SEEK_END);
  size=ftell(fp)/sizeof(complex double); //complex numbers
  rewind(fp);

  // Allocate buffer
  assert(*buf=malloc(sizeof(complex double)*size));
  
  // Read values into buffer
  assert((fread(*buf,size*sizeof(complex double),1,fp))==1);

  fclose(fp);
  // Return number of elements
  return size;
}

int
mandelwrite(int count,complex double *buf,char *filename)
{
  // mandelread reads an unknown number of data points in binary
  // format into a buffer

  FILE *fp;
  int size=count*sizeof(complex double),wsize;
  assert(fp=fopen(filename,"wb"));
  
  // write values into buffer
  assert((fwrite(buf,size,1,fp))==1);
  
  fclose(fp);
  // Return number of elements
  return count;
}

