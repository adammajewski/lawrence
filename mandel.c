// Copyright 2015, Piers Lawrence. See LICENSE for licensing details.
#include<complex.h>

complex double
mandel(int k,complex double z)
{
  /* mandel is a function to evaluate the mandelbrot polynomial of
     level k, and where:

     degree(p[k])=2^(k-1)-1     
   */
  int i;
  complex double p;
  p = 1.0;
  for(i=1;i<=k-1;i++)
    {
      p = z*p*p+1.0;
    }
  return p;
}

int
mandeld(int k,complex double z,complex double *pout,complex double *dpout)
{
  /* Evaluate mandelbrot polynomial and its derivative */
  int i;
  complex double p,dp;

  p =1.0;
  dp = 0.0;
  for(i=1;i<=k-1;i++)
    {
      dp = p*p+2.0*z*p*dp;
      p = z*p*p+1.0;
    }
  
  *pout = p;
  *dpout = dp;
  return 0;
}

int
mandeld3(int k,complex double z,complex double *pout,complex double *dpout,\
        complex double *d2pout,complex double *d3pout)
{
  /* Evaluate mandelbrot polynomial and its derivative */
  int i;
  complex double p,dp,d2p,d3p;

  p =1.0;
  dp = 0.0;
  d2p=0.0;
  d3p=0.0;
  for(i=1;i<=k-1;i++)
    {
      d3p=6.0*(dp*dp+p*d2p+z*dp*d2p)+2.0*z*p*d3p;
      d2p=(4.0*p+2*z*dp)*dp+2.0*z*d2p;
      dp = p*p+2.0*z*p*dp;
      p = z*p*p+1.0;
    }
  
  *pout = p;
  *dpout = dp;
  *d2pout = d2p;
  *d3pout = d3p;
  return 0;
}
