  // This block reads in an unknown number of initial values into the
  // buffer given by the input argument -f name
  fpi=fopen(iname,"r");
  buflen=BUFLEN;
  assert(startbuf=malloc(sizeof(complex double)*buflen));
  nlines=0;
  while((c=fscanf(fpi,"%lf %lf",&re,&im))!=EOF)
    {
      startbuf[nlines]=re+I*im;
      nlines++;
      if(nlines>=buflen)
        {
          // Double buffer length and reallocate memory
          buflen *= 2;
          assert(startbuf=realloc(startbuf,sizeof(complex double)*buflen));
        }
    }
  fclose(fpi);

  fpo=fopen(oname,"wb");
  fwrite(zstar,sizeof(complex double),2*nlines+1,fpo);
  fclose(fpo);

  /* fpo=fopen(oname,"w"); */
  /* for(i=0;i<nlines;i++) */
  /*   { */
  /*     fprintf(fpo,"%.16e %.16e\n%.16e %.16e\n",creal(zstar[2*i]),cimag(zstar[2*i]),\ */
  /*             creal(zstar[2*i+1]),cimag(zstar[2*i+1])); */
  /*   } */
  /* fprintf(fpo,"%.16e %.16e\n",creal(zstar[2*nlines]),cimag(zstar[2*nlines])); */
  
  /* fclose(fpo); */
